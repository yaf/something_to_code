require 'rubygems'
require 'json'
require 'net/http'

selected_letter = ('a'..'z').to_a[rand(23)]
puts "selected letter: #{selected_letter}"

base_url = "http://github.com/api/v2/json/repos/search/#{selected_letter}"
#url = "#{base_url}"
#resp = Net::HTTP.get_response(URI.parse(url))
#data = resp.body

url = URI.parse(base_url)
req = Net::HTTP::Get.new(url.path + '?language=ruby')
res = Net::HTTP.start(url.host, url.port) {|http|
  http.request(req)
}

result = JSON.parse(res.body)

if result.has_key? 'Error'
  raise "web service error"
end

repos = result["repositories"]

puts "count repo before filter... : #{repos.size}"
repos = repos.select{|r| r["followers"] > 10}
puts "count repo after followers filter... : #{repos.size}"
#repos = repos.select{|r| r["pushed"] > }
puts "count repo after followers filter... : #{repos.size}"
 
selected_repo = repos[rand(repos.size)]

puts "name : #{selected_repo["name"]} followers : #{selected_repo["followers"]} username : #{selected_repo["username"]} pushed: #{selected_repo['pushed']}" 
