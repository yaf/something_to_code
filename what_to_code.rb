require 'rubygems'
require 'octopi'

selected_letter = ('a'..'z').to_a[rand(23)]
puts "selected letter: #{selected_letter}"
repos = Octopi::Repository.find_all(selected_letter)

puts "count repo before filter... : #{repos.size}"
repos = repos.select{|r| r.language == 'Ruby'}
puts "count repo after ruby filter... : #{repos.size}"
repos = repos.select{|r| r.followers > 10}
puts "count repo after followers filter... : #{repos.size}"

selected_repo = repos[rand(repos.size)]

puts "name : #{selected_repo.name} followers : #{selected_repo.followers} username : #{selected_repo.username}"

